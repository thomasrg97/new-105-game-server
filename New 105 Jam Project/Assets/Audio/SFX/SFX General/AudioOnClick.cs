using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AC;

public class AudioOnClick : MonoBehaviour
{
    public AudioSource source;
    public AudioClip audioToPlay;

    private void OnMouseDown()
    {
        source.clip = audioToPlay;
        source.loop = false;
        source.Play();
    }

} 






